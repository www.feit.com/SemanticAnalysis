package com.whos.sa.seg;

import java.io.Reader;
import java.util.List;

import com.chenlb.mmseg4j.Word;

public interface Segmentation {

	public List<Word> execute(Reader reader);

	public List<Word> execute(String s);
}
