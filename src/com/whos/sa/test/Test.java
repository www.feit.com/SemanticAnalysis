package com.whos.sa.test;

import com.whos.sa.analysis.Analysis;
import com.whos.sa.util.log.LogUtil;

public class Test {

	public static void main(String[] args) {
		LogUtil.getInstance().logInfo("Test SA");
		Analysis analysis = new Analysis();
		long s = System.currentTimeMillis();
		for (int i = 0; i < 500; i++) {
			System.out.println("1: " + analysis.parse("这是一个普通评论").getCode());
			System.out.println("2: " + analysis.parse("城管恶毒，不是人").getCode());
			System.out.println("3: " + analysis.parse("中国加油!").getCode());
			System.out.println("4: "
					+ analysis.parse("不要过于相信开源，有些东西是不会公开的。!").getCode());
			System.out.println("5: "
					+ analysis.parse("这特么你也信java。。。注定孤独终老。。").getCode());
			System.out
					.println("6: " + analysis.parse("政府相关部门应当介入了！").getCode());
			System.out.println("7: "
					+ analysis.parse("教育腐败何时查　没人管了吗？").getCode());
			System.out.println("8: " + analysis.parse("很棒，好文章").getCode());
		}
		long e = System.currentTimeMillis();
		System.out.println(e - s);
	}
}
