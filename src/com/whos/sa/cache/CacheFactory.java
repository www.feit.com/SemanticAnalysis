package com.whos.sa.cache;

import com.whos.sa.cache.impl.CrawlCacheManagerImpl;

public class CacheFactory {

	private static CrawlCacheManager cm = new CrawlCacheManagerImpl();

	private CacheFactory() {

	}

	public static CrawlCacheManager getCacheManager() {
		return cm;
	}
}
